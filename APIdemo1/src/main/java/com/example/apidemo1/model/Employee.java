package com.example.apidemo1.model;

import jakarta.persistence.*;

@Entity
@Table(name = "employee")
public class Employee {
    // tạo database
    @Id
    // id tăng dần
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long  id;
    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;

    // khai bao getter vs setter và constructer
    public Employee(long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
    public Employee(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
