package com.example.apidemo1.service;

import com.example.apidemo1.model.Employee;

import java.util.List;

public interface IEmployeeService {
    // ham them nhan vien
    public Employee addEmployee(Employee employee);

    //ham cin sua thong tin
    public Employee updateEmployee(long id,Employee employee);

    // ham xoa nhan vien
    public boolean deleteEmployee(long id);

    // ham la ra danh sach nhan vien
    public List<Employee> getAllEmployee();

    // ham lay ra 1 nhan vien
    public Employee getOneEmployee(long id);
}
